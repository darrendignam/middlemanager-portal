<?php

create variable sid char(20);

//Start off and get an SID (Session ID)
set sid=(select spaceman.WContractInit( 
            (select siteid from site where sitepin='PK'),                           //isiteid
            (select customerid from customer where customernumber='930'),           //icustomerid
            dateadd(month,1,(select startupdate from site where sitepin='PK'))-1,   //itodate
            null,                                                                   //ireservationid
            1,                                                                      //iInvFreq
            'M')                                                                    //iInvPeriod
);

// Deposit for new contract?
if spaceman.WContractDetail(
        sid,                                                    //isessionid
        (select startupdate from site where sitepin='PK'),      //iStartDate
        100,                                                    //iInitialAmount
        (select vatcode from ledgeritem join sizecoderate join unit where unitnumber='A1'), //iVATCode
        (select unitid from unit where unitnumber='A1'),        //iUnitid
        null,                                                   //iLedgeritemid
        100,                                                    //iDeposit
        1,                                                      //iQuantity
        100,                                                    //iRateAmount
        null,null,null,null,null,null,                          //itimezone // ikeypadzone // ipassword // idiscountschemeid // iDiscount // iDiscountUntil
        1,                                                      //iAutoDiscount
        8000,                                                   //iGoodsValue
        'This is the additional text'                           //iAdditional
    ) <> sid then 
                message('issue1') 
end if;

// Insurance (£8000) (I1)
if spaceman.WContractDetail(
        sid,                                                            //isessionid
        (select startupdate from site where sitepin='PK'),              //iStartDate
        8,                                                              //iInitialAmount
        (select vatcode from ledgeritem where ledgeritem='I1'),         //iVATCode
        (select unitid from unit where unitnumber='A1'),                //iUnitid
        (select ledgeritemid from ledgeritem where ledgeritem='I1'),    //iLedgeritemid
        0,                                                              //iDeposit
        4000,                                                           //iQuantity
        8,                                                              //iRateAmount
        null,null,null,null,null,null,                                  //itimezone // ikeypadzone // ipassword // idiscountschemeid // iDiscount // iDiscountUntil
        1,                                                              //iAutoDiscount
        0,                                                              //iGoodsValue
        'This is insurance'                                             //iAdditional
    ) <> sid then 
        message('issue2') 
end if;

//Padlock Hire (h1)
if spaceman.WContractDetail(
        sid,                                                            //isessionid
        (select startupdate from site where sitepin='PK'),              //iStartDate
        (select monthrate from ledgeritem where ledgeritem='H1')*2,     //iInitialAmount
        (select vatcode from ledgeritem where ledgeritem='H1'),         //iVATCode
        (select unitid from unit where unitnumber='A1'),                //iUnitid
        (select ledgeritemid from ledgeritem where ledgeritem='H1'),    //iLedgeritemid
        0,                                                              //iDeposit
        2,                                                              //iQuantity
        (select monthrate from ledgeritem where ledgeritem='H1')*2,     //iRateAmount
        null,null,null,null,null,null,                                  //itimezone // ikeypadzone // ipassword // idiscountschemeid // iDiscount // iDiscountUntil
        1,                                                              //iAutoDiscount
        0,                                                              //iGoodsValue
        null                                                            //iAdditional
    ) <> sid then
        message('issue3') 
end if;

//Blankets bought 
if spaceman.WContractDetail(
        sid,                                                            //isessionid
        (select startupdate from site where sitepin='PK'),              //iStartDate
        (select price from ledgeritem where ledgeritem='S490')*3,       //iInitialAmount
        null,                                                           //iVATCode
        null,                                                           //iUnitid
        (select ledgeritemid from ledgeritem where ledgeritem='S490'),  //iLedgeritemid
        0,                                                              //iDeposit
        3,                                                              //iQuantity
        null,                                                           //iRateAmount
        null,null,null,null,null,null,                                  //itimezone // ikeypadzone // ipassword // idiscountschemeid // iDiscount // iDiscountUntil
        null,null,                                                      //iAutoDiscount // iGoodsValue
        null                                                            //iAdditional
    ) <> sid then 
        message('issue4') 
end if;

//submit new contract
select spaceman.WContractPost(
    sid,                                //isessionid
    'C1',                               //ipaymethod      char(4)     - Payment Method ID. ‘C1’, ‘C2’ etc. //not sure where to get these
    'CASH-PAY',                         //ipayref         char(12)    - Payment reference
    200                                 //ipayamount      double      - Payment Amount
);
//this select will return 'Invalid' for a failure, or '0' for success
